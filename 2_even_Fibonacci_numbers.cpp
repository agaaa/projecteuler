#include<iostream>
#include<cmath>

using namespace std;

int logarithm(unsigned long long value, float base) {
  return log(value) / log(base);
}

// it won't work for the small limit values!
unsigned long long even_fibbonaci_numbers_sum(unsigned long long limit) {
  int n = logarithm(limit * sqrt(5), (1+sqrt(5))/2); // the biggest index of fibonacci number which value doesn't exceed the limit
  n -= n%3; // the biggest index divided by 3 (even value) of fibonacci number which value doesn't exceed the limit 
  return (pow(((1+sqrt(5))/2),n+2) - 1)/sqrt(5)/2;
}

int main(int argc, char *argv[]) {
  
  cout << "Sum of the even-valued Fibbonaci numbers whose do not exceed four million equals: " << 
    even_fibbonaci_numbers_sum(4000000) << endl;
  
  return 0;
}
