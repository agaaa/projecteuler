#include<iostream>

using namespace std;

int sum_of_multiples(int value, int limit) {
  int num_of_multiples = (limit - 1) / value;
  return num_of_multiples * (num_of_multiples + 1) / 2 * value;
}

int main(int argc, char *argv[]) {

  int val1 = 3, val2 = 5, limit = 1000;
  cout << "Sum of multiples of 3 and 5 below 1000: " << 
    sum_of_multiples(val1, limit) + sum_of_multiples(val2, limit) - sum_of_multiples(val1 * val2, limit) << endl;

  return 0;
}
